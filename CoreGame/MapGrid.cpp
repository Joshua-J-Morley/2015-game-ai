 /**
 * @class MapGrid
 * @file MapGrid.cpp
 * Class for map, contains 2D array of @ref Environment
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref MapGrid.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "MapGrid.h"
#include <sstream>
#include <string.h>
#include <stdio.h>
/*#include <random>*/

/**
 * @brief      Default Constructor
 */
MapGrid::MapGrid()
{
	length = 0;
	height = 0;
	numPits = 0;
	numBlank = 0;

	/*grid = NULL;
	hero = NULL;*/
	/*hero = Hero(0, 0, 1, 1);*/
}

/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inUserID  users ID
 */
MapGrid::MapGrid(int inUserID)
{
	length = 0;
	height = 0;
	numPits = 0;
	numBlank = 0;
	
	/*grid = NULL;*/
	/*hero = Hero(0, 0, inUserID, 1);*/
}

/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX       x coordinate of user
 * @param[in]  inY       y coordinate of user
 * @param[in]  inUserID  users ID
 */
MapGrid::MapGrid(int inX, int inY, int inUserID)
{
	length = 0;
	height = 0;
	numPits = 0;
	numBlank = 0;
	
	/*grid = NULL;*/
	/*hero = Hero(inX, inY, inUserID, 1);*/
}


/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX           x coordinate of user
 * @param[in]  inY           y coordinate of user
 * @param[in]  inUserID      users ID
 * @param[in]  inHeroHealth  heroes health
 */
MapGrid::MapGrid(int inX, int inY, int inUserID, int inHeroHealth)
{
	length = 0;
	height = 0;
	numPits = 0;
	numBlank = 0;
	
	grid = NULL;
	/*hero = Hero(inX, inY, inUserID, inHeroHealth);*/
}

/**
 * @brief      virtual Destructor
 */
MapGrid::~MapGrid()
{
	//make sure grids not null
	if (grid != NULL && height > 0)
	{
		//for every row
		for (int ii = 0; ii < height+2; ii++)
		{
			//for every column
			for (int jj = 0; jj < length+2; jj++)
			{
				//ensure elements not null
				if (grid[ii][jj] != NULL)
				{
					//delete individual element
					delete grid[ii][jj];
					//nullify dangling pointer
					grid[ii][jj] = NULL;
				}
			}
			delete[] grid[ii];
		}
		delete[] grid;
	}
}


/**
 * @brief      accessor for 2D array of @ref Environment
 *
 * @return     @ref Environment pointer to 2D array
 */
Environment ***MapGrid::getMapGrid()
{
	return grid;
}

Environment *MapGrid::getTile(int inXCoord, int inYCoord)
{
	coordinateToAbsolute(&inXCoord, &inYCoord);

	return grid[inYCoord][inXCoord];
}

/**
 * @brief      Function to autogenerate a map.
 */
void MapGrid::generateMapGrid()
{
	int min, max;
	srand(time(NULL));

	/*printf("Enter Max grid size\n");
	scanf("%d", &max);

	printf("Enter Min grid size\n");
	scanf("%d", &min);*/

	//length = (randomGenerator() % max) + min;
	

	length = 3; //hardcoded temporarily
	height = 3;

	grid = new Environment**[height+2];//+2 for boundary
	for (int ii = 0; ii < length+2; ii++)
	{
		grid[ii] = new Environment*[length+2];
	}

	createBorder();

	placePits();

	spawnGold();

	spawnWumpus();
	
}

/**
 * @brief      function creates border around grid, and fills rest of grid blank
 * @todo refactor this for new structure
 */
void MapGrid::createBorder()
{
	for (int ii = 0; ii < height+2; ii++)
	{
		for (int jj = 0; jj < length+2; jj++)
		{
			int xCoord = jj, yCoord = ii;
			absoluteToCoordinate(&xCoord, &yCoord);

			if (ii == 0)//upper boundary
			{
				//set all upper boundary of grid to impassable terrain
				grid[ii][jj] = new ImpassableTerrain(xCoord, yCoord);
				/*grid[ii][jj]->addObject()*/
			}
			else if (jj == 0)//left boundary
			{
				//set all left boundary of grid to impassable terrain
				grid[ii][jj] = new ImpassableTerrain(xCoord, yCoord);
			}
			else if (jj == length+1) //right boundary
			{
				//set all right boundary of grid to impassable terrain
				grid[ii][jj] = new ImpassableTerrain(xCoord, yCoord);
			}
			else if (ii == height+1)//bottom boundary
			{
				//set all bottom boundary of grid to impassable terrain
				grid[ii][jj] = new ImpassableTerrain(xCoord, yCoord);
			}
			else//blank terrain
			{
				//initialise rest of terrain to blank
				grid[ii][jj] = new Environment(xCoord, yCoord);
				numBlank ++;
			}
			/*printf("x=%d\ty=%d\n", xCoord,yCoord);*/
		}
	}
}

/**
  * @brief      Function evaluates how many Pits and places them randomly
  * @todo validate input from user
  *
  */ 
int MapGrid::placePits()
{
	int choice, minPits, maxPits, success = 1, environmentsChecked = 0;
	


	printf("How many pits to insert?\n");
	printf("1: exact amount\n");
	printf("2: In range\n");
	printf("3: Proportionate to mapsize\n");
	printf("Value: ");

	scanf("%d", &choice);

	switch (choice)
	{
		//if user wants specific number of pits
		case 1:
			printf("please enter amount:\n");
			scanf("%d", &numPits);
			break;

		//number of pits random within range
		case 2:
			printf("please enter range: \"min:max\"\n");
			scanf("%d:%d", &minPits, &maxPits);
			//sets upper and lower boundary for random			
			numPits = (rand() % (maxPits-minPits)) + minPits;
			break;

		//proportionate to mapsize
		case 3:
			//1/5th hardcoded temporarily, can be changed
			numPits = (length*height)/5;
			printf("%d pits will be placed\n", numPits);
			break;

		//can change default at later stage
		default:
			numPits = (length*height)/5;
			break;
	}

	//place number of pits desired
	int pitsLeft = numPits;
	while (pitsLeft > 0 && success != 0)
	{
		//randomly generate x and y coordinate to place pit
		int pitXCoord = (rand() % (length-1))+1;
		int pitXAbsCoord = pitXCoord;

		
		int pitYCoord = (rand() % (height-1))+1;
		int pitYAbsCoord = pitYCoord;

		//absolute index needed for array lookup
		coordinateToAbsolute(&pitXAbsCoord, &pitYAbsCoord);

		//if the element at those index's is a blank terrain, delete blank terrain
		// and place pit terrain there
		if (dynamic_cast <Environment *> (grid[pitYAbsCoord][pitXAbsCoord]) )
		{
			delete grid[pitYAbsCoord][pitXAbsCoord];
			grid[pitYAbsCoord][pitXAbsCoord] = new Pit(pitXCoord, pitYCoord);
			grid[(pitYAbsCoord+1)][pitXAbsCoord]->addPerception(Breeze());
			grid[(pitYAbsCoord-1)][pitXAbsCoord]->addPerception(Breeze());
			grid[pitYAbsCoord][(pitXAbsCoord+1)]->addPerception(Breeze());
			grid[pitYAbsCoord][(pitXAbsCoord-1)]->addPerception(Breeze());

			printf("pit was placed:[%d, %d]\n",pitXCoord,pitYCoord);

			//only decrement if pit is placed
			pitsLeft --;
			numBlank --;

		}
		else if (numBlank == 0)//if there is no more blank spaces on grid
		{
			//success is false
			success = 0;
		}
	}

	return success;
}

/**
 * @brief      function to place wumpus on a random blank environment
 * @todo combine with spawn wumpus rather than 2 methods, pass object in
 *
 * @return     success of spawn wumpus: will return 0 for fail, 1 for success
 */
int MapGrid::spawnGold()
{
	bool placed = false;
	int environmentsChecked = 0, success = 1;
	

	while (placed == false && success != 0)
	{
		//randomly generate x and y coordinate to place wumpus
		int goldXCoord = (rand() % (length-1))+1;
		int goldxAbs = goldXCoord;

		
		int goldYCoord = (rand() % (height-1))+1;
		int goldyAbs = goldYCoord;


		//absolute index needed for array lookup
		coordinateToAbsolute(&goldxAbs, &goldyAbs);

		//if element at those index's is a blank terrain, delete blank terrain
		// and place gold there
		if (dynamic_cast <Environment *> (grid[goldyAbs][goldxAbs]) )
		{
			grid[goldyAbs][goldxAbs]->addObject(Gold(goldXCoord, goldYCoord));
			grid[(goldyAbs)][goldxAbs]->addPerception(Glitter());

			printf("Gold was placed:[%d, %d]\n", goldXCoord, goldYCoord);

			//indicate gold has been placed
			placed = true;
			numBlank --;
			/*printf("numblank: %d\n", numBlank);*/
		}
		else if (numBlank == 0)
		{
			success = 0;
		}
	}

	return success;
}

/**
 * @brief      function to place wumpus on a random blank environment
 * @todo combine with spawn gold rather than 2 methods, pass object in
 *
 * @return     success of spawn wumpus: will return 0 for fail, 1 for success
 */
int MapGrid::spawnWumpus()
{
	bool placed = false;
	int environmentsChecked = 0, success = 1;
	

	while (placed == false && success != 0)
	{
		//randomly generate x and y coordinate to place wumpus
		int wumXCoord = (rand() % (length-1))+1;
		int wumXAbs = wumXCoord;

		
		int wumYCoord = (rand() % (height-1))+1;
		int wumYAbs = wumYCoord;

		//absolute index needed for array lookup
		coordinateToAbsolute(&wumXAbs, &wumYAbs);

		//if element at those index's is a blank terrain, delete blank terrain
		// and place wumpus there
		if (dynamic_cast <Environment *> (grid[wumYAbs][wumXAbs]) )
		{
			grid[wumYAbs][wumXAbs]->addObject(Wumpus(wumXCoord, wumYCoord));
			grid[wumYAbs+1][wumXAbs]->addPerception(Stench());
			grid[wumYAbs-1][wumXAbs]->addPerception(Stench());
			grid[wumYAbs][wumXAbs+1]->addPerception(Stench());
			grid[wumYAbs][wumXAbs-1]->addPerception(Stench());


			printf("wumpus was placed:[%d, %d]\n", wumXCoord, wumYCoord);

			printf("Stench was placed:[%d, %d]\n", wumXCoord+1, wumYCoord);
			printf("Stench was placed:[%d, %d]\n", wumXCoord-1, wumYCoord);
			printf("Stench was placed:[%d, %d]\n", wumXCoord, wumYCoord+1);
			printf("Stench was placed:[%d, %d]\n", wumXCoord, wumYCoord-1);

			//indicate wumpus has been placed
			placed = true;
			numBlank --;
		}
		else if (numBlank == 0)
		{
			success = 0;
		}
	}

	return success;
}


/**
 * @brief      get the dimensions of the map
 *
 * @param      outLength  length of map returned by reference
 * @param      outHeight  height of map return ed by reference
 *
 * @return     { description_of_the_return_value }
 */
int MapGrid::getGridDimensions(int *outLength, int *outHeight)
{
	*outLength = length;
	*outHeight = height;
}


/**
 * @brief         convert coordinates (0,0 is bottom left) to array index's
 *
 * @param[in,out] outXCoord  import: coord of x, export: outer array index
 * @param[in,out] outYCoord  import: coord of y, export: inner array index
 *
 * @return        error handling, in case of null pointer return 0
 * 
 */
int MapGrid::coordinateToAbsolute(int *outXCoord, int *outYCoord)
{
	int success = 0;

	if (outXCoord != NULL && outYCoord != NULL)
	{
		*outXCoord = *outXCoord + 1;
		*outYCoord = height - *outYCoord;
		success = 1;
	}
	else
	{
		success = 0;
	}

	return success;
}

/**
 * @brief         convert array index's to coordinates (0,0 is bottom left)
 *
 * @param[in,out] outXCoord  import: outer array index, export: coord of x
 * @param[in,out] outYCoord  import: inner array index, export: coord of y
 *
 * @return        error handling, in case of null pointer return 0
 * 
 * @todo the coordinate to absolute doesnt seem to be working
 */
int MapGrid::absoluteToCoordinate(int *outXCoord, int *outYCoord)
{
	int success = 0;

	if (outXCoord != NULL && outYCoord != NULL)
	{
		*outXCoord = *outXCoord - 1;
		*outYCoord = height - *outYCoord;
		success = 1;
	}
	else
	{
		success = 0;
	}

	return success;
}


/**
 * @brief      Convert 2D MapGrid of Environments to string
 *
 * @return     string containing grid elements
 * 
 * @todo refactor this for new hierarchy, make it work :/
 * @todo it now works, look into implementing better, IE toString in envrironment
 * @todo update documenttaion and request client feedback
 */
char *MapGrid::toString()
{
	/*
	 * to string method
	 * conceptual:
	 * <hero x>,<hero y>|<grid length>,<grid height> <grid area>|
	 * <elements x>,<elements y>:<element type>|
	 * 
	 * legend:
	 * hero x 			The array index of hero
	 * hero y 			The inner array index of hero
	 * grid length 		length of grid + 2 for boundaries
	 * grid height 		height of grid + 2 for boundaries
	 * grid area		(length+2)*(height+2) IE the number of elements to parse
	 * 						+2 are for the boundaries
	 * for every element of grid (IE each tile or square)
	 * elements x 		The array index of this element
	 * elements y 		The inner array index of this element
	 * element type: (numbers for expandability)
	 * 					0 for Impassable Terrain
	 * 					1 for Pit
	 * 					2 for Blank Environment
	 * 					3 for Gold
	 * 					4 for Wumpus
	 * 					5 for Spawn
	 * 
	 * example alligned for readbaility: 
	 * 2,3|5,5,25|0,0:0|1,0:0|2,0:0|3,0:0|4,0:0
	 * 		     |0,1:0|1,1:2|2,1:1|3,1:3|4,1:0
	 * 		     |0,2:0|1,2:1|2,2:1|3,2:4|4,2:0
	 * 		     |0,3:0|1,3:5|2,3:1|3,3:1|4,3:0
	 * 		     |0,4:0|1,4:0|2,4:1|3,4:0|4,4:0|
	 * 
	 * grid would look like this 
	 *   ___ ___ ___ ___ ___
	 *  | I | I | I | I | I |
	 *  | I | P |   | G | I |
	 *  | I |   |   | W | I |
	 *  | I | S |   |   | I |
	 *  | I | I | I | I | I |
	 *   ___ ___ ___ ___ ___
	 *  
	*/
	std::stringstream mapString;
	char* retString;
	int xCoord, yCoord;

	const char pipeChar = '|';
	const char commaChar = ',';
	const char colonChar = ':';
	const char ampersandChar = '&';
	int numElements = (height+2)*(length+2);
	int height2 = height+2;
	int length2 = length+2;

	/*mapString.str(":");*/

	//add heroes x and y position to mapString
/*	mapString << hero.getXCoord() << commaChar;
	mapString << hero.getYCoord() << pipeChar;*/

	//add length and height +2 for boundaries
	mapString << height2 << commaChar;
	mapString << length2 << commaChar;

	//add number of elements including boudnary
	mapString << numElements << pipeChar; 

	mapString << '\n';
	for (int ii = 0; ii < height+2; ii++)
	{
		/*printf("%s\n", mapString.str());*/
		for (int jj = 0; jj < length+2; jj++)
		{
			xCoord = jj;
			yCoord = ii;

			absoluteToCoordinate(&xCoord, &yCoord);

			

			if (dynamic_cast <ImpassableTerrain *> (grid[ii][jj]) )
			{
				mapString << '0' << commaChar;
				mapString << '0' << colonChar;

				mapString << '0' << pipeChar;
			}
			else if (dynamic_cast <Pit *> (grid[ii][jj]) )
			{
				mapString << xCoord << commaChar;
				mapString << yCoord << colonChar;
				
				mapString << 'P' << pipeChar;
			}
			else if (dynamic_cast <Environment *> (grid[ii][jj]) )
			{
				mapString << xCoord << commaChar;
				mapString << yCoord << colonChar;

				char * tileObjects = grid[ii][jj]->getObjects();
				/*printf("object:%s||\t", tileObjects);*/
				if (strlen(tileObjects) > 0)
				{
					
					for (int kk = 0; kk < strlen(tileObjects); kk++)
					{
						printf("objectss in hereeee: %s\n", tileObjects);
						mapString << tileObjects[kk];

						if (kk + 1 < strlen(tileObjects))
						{
							mapString << ampersandChar << ampersandChar;
						}
					}
					mapString << pipeChar;
				}
				else
				{
					mapString << '2' << pipeChar;
				}
			}
			else
			{
				printf("%s\n", "something went wrong");
				//something went wrong
			}
			
		}
		mapString << '\n';
	}
	retString = (char*)malloc(sizeof((mapString.str().c_str()))*sizeof(char)+1);

	strcpy(retString, mapString.str().c_str());

	printf("%s\n", retString);

	return retString;

}
