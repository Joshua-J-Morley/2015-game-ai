/**
 * @class Behaviour
 * @file Behaviour.cpp
 * Base class for abehaviours of actor or world
 * Inherited by 
 * - @ref Percept
 * - @ref Action
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref Behaviour.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "Behaviour.h"

/**
 * @brief      Default Constructor
 */
Behaviour::Behaviour ()
{
	
}



/**
 * @brief      Destructor
 */
Behaviour::~Behaviour ()
{

}

/**
 * @brief      function to be overwritten by subclasses
 */
int Behaviour::tick ()
{

}

/**
 * @brief      function to be overwritten by subclasses
 */