 /**
 * @class Shoot 
 * @file Shoot.h
 * class for implementing shoot actuator
 * Inherits from @ref Action
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement functionality
 */

#ifndef SHOOT_HEADER
#define SHOOT_HEADER

#include "Action.h"

class Shoot : public Action
{
	public:
		Shoot ();
		virtual ~Shoot();
		
		int shootArrow(int *xCoord, int *yCoord, int directionFaced);
		int isHit(char *items);
		int attemptAction();
		int tick ();
};

#endif /*SHOOT_HEADER*/