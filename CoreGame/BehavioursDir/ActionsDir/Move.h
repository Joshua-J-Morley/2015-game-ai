/**
 * @class Move 
 * @file Move.h
 * class for implementing moving forward
 * Inherits from @ref Action
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date sep 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement functionality for movement through temporary moveOne
 */

#ifndef MOVE_HEADER
#define MOVE_HEADER

#include "Action.h"

class Move : public Action
{
	public:
		Move ();
		virtual ~Move();
		
		int moveOne(int *xCoord, int *yCoord, int directionFaced);
		int attemptAction();
		int tick ();
};

#endif /*MOVE_HEADER*/