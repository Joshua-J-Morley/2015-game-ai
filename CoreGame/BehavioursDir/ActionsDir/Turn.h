/**
 * @class Turn 
 * @file Turn.h
 * class for implementing moving forward
 * Inherits from @ref Action
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: implement functionality
 */

#ifndef TURN_HEADER
#define TURN_HEADER

#include "Action.h"

class Turn : public Action
{
	int directionFaced;

	public:
		Turn ();
		virtual ~Turn();
		
		int turnLeft (int inDirectionFaced);
		int turnRight (int inDirectionFaced);
		void newDirection(int inDirectionFaced);
		int attemptAction();
		int tick ();
};

#endif /*TURN_HEADER*/