/**
 * @class Grab
 * @file Grab.h
 * class for implementing grab actuator
 * Inherits from @ref Action
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date sep 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement Grab Functionality
 */

#ifndef GRAB_HEADER
#define GRAB_HEADER

#include <string.h>
#include "Action.h"
 class MapGrid;
class Character;
class Environment;

class Grab : public Action
{
	public:
		Grab ();
		virtual ~Grab();

		int grabItem (char *items);
		/*int attemptAction (Hero *inHero);*/
		int attemptAction ();
		int tick ();
};

#endif /*GRAB_HEADER*/