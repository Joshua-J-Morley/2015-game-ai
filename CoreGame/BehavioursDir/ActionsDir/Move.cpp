 /**
 * @class Move 
 * @file Move.cpp
 * class for implementing moving forward
 * Inherits from @ref Action
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref Move.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "Move.h"

/**
 * @brief      Default Constructor
 */
Move::Move () : Action ()
{

}

/**
 * @brief      Destructor
 */
Move::~Move()
{

}

/**
 * @brief      function to implement movement until polymorphic approach
 *
 * @param      xCoord          heroes x coordinate updated by reference
 * @param      yCoord          heroes y coordinate updated by reference
 * @param[in]  directionFaced  the direction hero is facing
 *
 * @return     result of attempt movement
 */
int Move::moveOne(int *xCoord, int *yCoord, int directionFaced)
{
	switch(directionFaced)
	{
		case 1:
			(*yCoord)++;
			break;
		case 2:
			(*xCoord)++;
			break;
		case 3:
			(*yCoord)--;
			break;
		case 4:
			(*xCoord)--;
			break;
		default:
			break;
	}

	
}

/**
 * @brief      Moves implementation of attempt action.
 *
 * @return     Result of attempting Move.
 */
int Move::attemptAction()
{

}

/**
 * @brief      function for performing action when relevant
 *
 * @return     Result of tick
 */
int Move::tick ()
{

}