/**
 * @class Action
 * @file Action.h
 * Base class for actions that can be performed in the Wumpus World
 * Inherits from @ref Behaviour
 * Inherited by 
 * - @ref Grab
 * - @ref Shoot
 * - @ref Turn
 * - @ref Move
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date sep 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0.1
 * @date Oct 2015
 * @brief Reason: Implement some Action functionalities
 */

#ifndef ACTION_HEADER
#define ACTION_HEADER

#include <stdio.h>
#include <string.h>
#include "../Behaviour.h"

/*class Hero;*/

class Action : public Behaviour
{
	public:
		Action ();
		virtual ~Action ();

		/*virtual int attemptAction (Hero *inHero);*/
		virtual int attemptAction ();
		virtual int tick ();
};

#endif /*ACTION_HEADER*/