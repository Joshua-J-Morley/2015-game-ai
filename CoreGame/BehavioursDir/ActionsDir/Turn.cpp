 /**
 * @class Turn 
 * @file Turn.cpp
 * class for implementing moving forward
 * Inherits from @ref Action
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref Turn.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "Turn.h"

/**
 * @brief      Default Constructor
 */
Turn::Turn () : Action ()
{
	/*1 is 'up', 2 is  is 'right', 3 is 'down' 4 is 'left'*/
	directionFaced = 1;
}


/**
 * @brief      Destructor
 */
Turn::~Turn()
{

}

int Turn::turnLeft (int inDirectionFaced)
{
	int outDirectionFaced = inDirectionFaced;

	if (inDirectionFaced == 1)
	{
		outDirectionFaced = 4;
	}
	else
	{
		outDirectionFaced--;
	}

	newDirection(outDirectionFaced);

	return outDirectionFaced;
}

int Turn::turnRight (int inDirectionFaced)
{
	int outDirectionFaced = inDirectionFaced;
	
	if (inDirectionFaced == 4)
	{
		directionFaced = 1;
	}
	else
	{
		outDirectionFaced++;
	}

	newDirection(outDirectionFaced);

	return outDirectionFaced;
}

void Turn::newDirection(int inDirectionFaced)
{
	printf("Your hero mysteriously stares into the ");
	switch(inDirectionFaced)
	{
		case 1:
			printf("North\n");
			break;
		case 2:
			printf("East\n");
			break;
		case 3:
			printf("South\n");
			break;
		case 4:
			printf("West\n");
			break;
		default:
			break;
	}
}

/**
 * @brief      Turns implementation of attempt action.
 *
 * @return     Result of attempting Turn.
 */
/*int Turn::attemptAction(Hero *inHero)
{
	inHero->setDirection(turnLeft(inHero->getDirection()));
}*/
int Turn::attemptAction ()
{
	
}


/**
 * @brief      function for performing action when relevant
 *
 * @return     Result of tick
 */
int Turn::tick ()
{

}