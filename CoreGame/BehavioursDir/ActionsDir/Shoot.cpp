/**
 * @class Shoot 
 * @file Shoot.cpp
 * class for implementing shoot actuator
 * Inherits from @ref Action
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref Shoot.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "Shoot.h"

/**
 * @brief      Default Constructor
 */
Shoot::Shoot () : Action ()
{

}

/**
 * @brief      Destructor
 */
Shoot::~Shoot()
{

}

/**
 * @brief      function to implement shooting until polymorphic approach
 *
 * @param      xCoord          x coordinate of hero, will return coordinate shot
 * @param      yCoord          y coordinate of hero, will return coordinate shot
 * @param[in]  directionFaced  direction hero is facing
 *
 * @return     result of action attempt
 */
int Shoot::shootArrow(int *xCoord, int *yCoord, int directionFaced)
{
	switch(directionFaced)
	{
		case 1:
			(*yCoord)++;
			break;
		case 2:
			(*xCoord)++;
			break;
		case 3:
			(*yCoord)--;
			break;
		case 4:
			(*xCoord)--;
			break;
		default:
			break;
	}

	
}

/**
 * @brief      called after a coordinate is recieved from shootArrow
 *
 * @param      items  string of items located at element being shot at
 *
 * @return     result of action attempt
 */
int Shoot::isHit(char *items)
{
	if (strstr(items, "W")!=NULL)
	{
		printf("Killed Wumpus!!\n\n");
	}
	else
	{
		printf("Missed Wumpus\n\n");	
	}
}

/**
 * @brief      Turns implementation of attempt action.
 *
 * @return     Result of attempting Turn.
 */
int Shoot::attemptAction()
{

}

/**
 * @brief      function for performing action when relevant
 *
 * @return     Result of tick
 */
int Shoot::tick ()
{

}