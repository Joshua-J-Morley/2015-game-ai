/**
 * @class Grab
 * @file Grab.cpp
 * class for implementing grab actuator
 * Inherits from @ref Action
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date sep 2015
 * 
 * For intermediate modifications see @ref Grab.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "Grab.h"

/**
 * @brief      Default Constructor
 */
Grab::Grab () : Action ()
{

}

/**
 * @brief      Destructor
 */
Grab::~Grab ()
{

}

/**
 * @brief      this function is the temp function to grab gold (if it exists)
 *
 * @param      items  { parameter_description }
 *
 * @return     { description_of_the_return_value }
 */
int Grab::grabItem (char *items)
{
	int result = 0;

	if (strstr(items, "G")!=NULL)
	{
		printf("FOUND GOLD!!\n\n");
		result = 1;
	}
	else
	{
		printf("No GOLD\n\n");	
		result = 0;
	}
	return result;
}


/**
 * @brief      this function will be the function called when iterating Actions
 *
 * @return     result of attempt
 * 
 *  At the moment this is commented out, more time was needed to implement
 *  the polymorphism approach for all of the actions
 *  -Josh
 */
int Grab::attemptAction ()
{
/*	int characXCoord = 0, characYCoord = 0, gridLength = 0, gridHight = 0;
	char* tileObjects;

	inCharacter.getCoordinates(&characXCoord, &characYCoord);
	inMapGrid.coordinateToAbsolute(&characXCoord, &characYCoord);

	
	tileObjects = grid[characYCoord][characXCoord]->getObjects();

	if (strstr(tileObjects, "G"))
	{
		printf("FOUND GOLD!!\n At:%d:%d\n\n", characYCoord, characXCoord);
	}*/
}



/**
 * @brief      function for performing action when relevant
 *
 * @return     Result of tick
 */
int Grab::tick ()
{

}

