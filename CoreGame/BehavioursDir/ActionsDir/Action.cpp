/**
 * @class Action
 * @file Action.cpp
 * Base class for actions that can be performed in the Wumpus World
 * Inherits from @ref Behaviour
 * Inherited by 
 * - @ref Grab
 * - @ref Shoot
 * - @ref Turn
 * - @ref Move
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref Action.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * 
 * @todo implment attemptAction polymorphically in each environment element
 */

#include "Action.h"

/**
 * @brief      Default Constructor
 */
Action::Action () : Behaviour ()
{

}

/**
 * @brief      Destructor
 */
Action::~Action()
{

}

/**
 * @brief      Method for subclasses to override and perform actions.
 *
 * @return     Result of attempt.
 */
/*int Action::attemptAction(Hero *inHero)
{

}*/
int Action::attemptAction()
{

}


/**
 * @brief      Method for subclasses to override and perform actions.
 *
 * @return     Result of attempt.
 */
int Action::tick()
{
	/*attemptAction(xCoord, yCoord);*/
}

