/**
 * @class Behaviour
 * @file Behaviour.h
 * Base class for abehaviours of actor or world
 * Inherited by 
 * - @ref Percept
 * - @ref Action
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: 
 */
#ifndef BEHAVIOUR_HEADER
#define BEHAVIOUR_HEADER


class Behaviour
{
	public:
		Behaviour ();
		virtual ~Behaviour ();
		virtual int tick ();

};

#endif /*BEHAVIOUR_HEADER*/