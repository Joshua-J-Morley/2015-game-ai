 /**
 * @class Scream
 * @file Scream.h
 * Class for Scream percept generated iff @ref Wumpus is killed anywhere in the @ref MapGrid
 * Inherits from @ref Percept
 * 
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 02 2015
 * 
 * @brief modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement perceptions into mocked functionality game
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement perceptions into mocked functionality game
 */

#ifndef SCREAM_HEADER
#define SCREAM_HEADER

#include <stdio.h>
#include "Percept.h"

class Scream : public Percept
{
	public:	
	    Scream();
	    virtual ~Scream();
	    char perceive();
	    int tick ();
};

#endif /*SCREAM_HEADER*/
