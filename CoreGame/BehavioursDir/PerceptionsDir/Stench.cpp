 /**
 * @class Stench
 * @file Stench.cpp
 * Class for Stench percept generated iff @ref Wumpus is in a neighbouring square @ref MapGrid
 * Inherits from @ref Percept
 * 
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 04 2015
 * 
 * For intermediate modifications see @ref Stench.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */


#include "Stench.h"

/**
 * @brief Default Constructor
 */
Stench::Stench() : Percept()
{

}

/**
 * @brief Destructor
 */
Stench::~Stench()
{

}

/**
 * @brief Displays Stench percept indicating a @ref Wumpus is in a neighbouring square in the @ref MapGrid
 */
char Stench::perceive()
{
    printf("A foul stench spoils the air\n");
    return 'H';
}

/**
 * @brief      inherits from @ref Behaviour will be used for polymorphism
 *
 * @return     result of tick
 */
int Stench::tick()
{
	perceive();
}