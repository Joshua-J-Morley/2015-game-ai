/**
 * @class Glitter
 * @file Glitter.h
 * Class for glitter percept iff gold is in current square @ref Gold
 * Inherits from @ref Percept
 * 
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 02 2015
 * 
 * @brief modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement perceptions into mocked functionality game
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement perceptions into mocked functionality game
 */

#ifndef GLITTER_HEADER
#define GLITTER_HEADER

#include "Percept.h"

class Glitter : public Percept
{
	public:	
		Glitter();
		virtual ~Glitter();
		char perceive();
		int tick ();
};

#endif /*GLITTER_HEADER*/
