 /**
 * @class Percept
 * @file Percept.h
 * Base class for perceptions that can be observed in the Wumpus World
 * Inherits from @ref Behaviour
 * Inherited by 
 * - @ref Breeze
 * - @ref Bump
 * - @ref Glitter
 * - @ref Scream
 * - @ref Stench
 * 
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 04 2015
 * 
 * @brief modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement perceptions into mocked functionality game
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement perceptions into mocked functionality game
 */


#ifndef AI_GAME_PROJECT_PERCEPT_H
#define AI_GAME_PROJECT_PERCEPT_H

#include "../Behaviour.h"

class Percept : public Behaviour
{
	public:
		Percept ();
		virtual ~Percept ();

		virtual char perceive ();
};

#endif //AI_GAME_PROJECT_PERCEPT_H
