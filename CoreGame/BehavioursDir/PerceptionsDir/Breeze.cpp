/**
 * @class Breeze
 * @file Breeze.cpp
 * class for the Breeze perception, detected iff a pit is in a neighbouring square
 * Inherits from @ref Percept
 * 
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 04 2015
 * 
 * For intermediate modifications see @ref Breeze.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */


#include <stdio.h>
#include "Breeze.h"

/**
 * @brief  Default Constructor
 */
Breeze::Breeze() : Percept()
{

}

/**
 * @brief Destructor
 */
Breeze::~Breeze()
{

}


/**
 * @brief Displays breeze perception as a message to the player indicating a @ref Pit is nearby
 */
char Breeze::perceive()
{
    printf("You feel a strong breeze\n");
    return 'Z';
}

/**
 * @brief      inherits from @ref Behaviour will be used for polymorphism
 *
 * @return     result of tick
 */
int Breeze::tick()
{
	perceive();
}