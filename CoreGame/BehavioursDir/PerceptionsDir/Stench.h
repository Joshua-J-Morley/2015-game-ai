/**
 * @class Stench
 * @file Stench.h
 * Class for Stench percept generated iff @ref Wumpus is in a neighbouring square @ref MapGrid
 * Inherits from @ref Percept
 * 
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 04 2015
 * 
 * @brief modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement perceptions into mocked functionality game
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement perceptions into mocked functionality game
 */

#ifndef STENCH_HEADER
#define STENCH_HEADER

#include <stdio.h>
#include "Percept.h"

class Stench : public Percept
{
	public:	
    	Stench();
    	virtual ~Stench();
    	char perceive();
    	int tick ();
};

#endif //STENCH_HEADER
