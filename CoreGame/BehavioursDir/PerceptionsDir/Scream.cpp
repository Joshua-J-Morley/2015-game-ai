 /**
 * @class Scream
 * @file Scream.cpp
 * Class for Scream percept generated iff @ref Wumpus is killed anywhere in the @ref MapGrid
 * Inherits from @ref Percept
 * 
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 04 2015
 * 
 * For intermediate modifications see @ref Scream.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "Scream.h"

/**
 * @brief Default Constructor
 */
Scream::Scream() : Percept()
{

}

/**
 * @brief Destructor
 */
Scream::~Scream()
{

}

/**
 * @brief Displays Scream perception when the player kills the @ref Wumpus
 */
char Scream::perceive()
{
    printf("You hear a harsh, shrill scream, the sound of a dying creature\n");
    return 'M';
}

/**
 * @brief      inherits from @ref Behaviour will be used for polymorphism
 *
 * @return     result of tick
 */
int Scream::tick()
{
	perceive();
}