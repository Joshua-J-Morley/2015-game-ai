/**
 * @class Bump
 * @file Bump.h
 * Inherits from @ref Percept
 * 
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 04 2015
 * 
 * @brief modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement perceptions into mocked functionality game
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement perceptions into mocked functionality game
 */

#ifndef BUMP_HEADER
#define BUMP_HEADER

#include "Percept.h"

class Bump : public Percept
{
	public:
		Bump();
		virtual ~Bump();
		char perceive();
		int tick ();
};


#endif //BUMP_HEADER
