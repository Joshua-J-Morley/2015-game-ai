 /**
 * @class Bump
 * @file Bump.cpp
 * Class for the Bump percept, displayed when player hits world boundary (@ref ImpassableTerrain)
 * Inherits from @ref Percept
 * 
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 04 2015
 * 
 * For intermediate modifications see @ref Bump.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */
#include <stdio.h>
#include "Bump.h"

/**
 * @brief Default Constructor
 */
Bump::Bump() : Percept()
{

}

/**
 * @brief Destructor
 */
Bump::~Bump()
{

}

/**
 * @brief Displays Bump perception to the player if they go forward into a wall, @ref ImpassableTerrain
 */
char Bump::perceive()
{
    printf("You bump up against the hard wall - you cannot go this way\n");
    return 'B';
}

/**
 * @brief      inherits from @ref Behaviour will be used for polymorphism
 *
 * @return     result of tick
 */
int Bump::tick()
{
	perceive();
}