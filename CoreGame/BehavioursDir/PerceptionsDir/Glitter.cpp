 /**
 * @class Glitter
 * @file Glitter.cpp
 * Class for glitter percept iff gold is in current square @ref Gold
 * Inherits from @ref Percept
 * 
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 04 2015
 * 
 * For intermediate modifications see @ref Glitter.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */
#include <stdio.h>
#include "Glitter.h"

/**
 * @brief Default Constructor
 */
Glitter::Glitter() : Percept()
{

}

/**
 * @brief Destructor
 */
Glitter::~Glitter()
{

}

/**
 * @brief Displays Glitter percept iff gold is in the same square as agent @ref Gold
 */
char Glitter::perceive()
{
    printf("You see a bright, golden glitter in the room\n");
    return 'G';
}

int Glitter::tick()
{
	perceive();
}