/**
 * @class Percept
 * @file Percept.cpp
 * Base class for actors actuators
 * inherits from @ref Behaviour
 * Inherited by 
 * - @ref Grab
 * - @ref Shoot
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref Percept.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "Percept.h"

/**
 * @brief      Default Constructor
 */
Percept::Percept () : Behaviour ()
{

}

/**
 * @brief      Destructor
 */
Percept::~Percept ()
{

}

/**
 * @brief      Method for subclasses to override and perform Percepts.
 *
 * @return     Result of attempt.
 */
char Percept::perceive ()
{

}

