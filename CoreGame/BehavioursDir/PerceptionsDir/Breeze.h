/**
 * @class Breeze
 * @file Breeze.h
 * class for the Breeze perception, detected iff a pit is in a neighbouring square
 * Inherits from @ref Percept
 * 
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 04 2015
 * 
 * @brief modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement perceptions into mocked functionality game
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement perceptions into mocked functionality game
 */

#ifndef BREEZE_HEADER
#define BREEZE_HEADER

#include "Percept.h"

class Breeze : public Percept
{
	public:
		Breeze();
		virtual ~Breeze();
		char perceive();
		int tick ();
	
};
#endif /*BREEZE_HEADER*/
