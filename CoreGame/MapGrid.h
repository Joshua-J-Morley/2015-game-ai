 /**
 * @class MapGrid
 * @file MapGrid.h
 * Class for map, contains 2D array of @ref Environment
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: 
 */

#ifndef MAP_GRID_HEADER
#define MAP_GRID_HEADER

#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sstream>
#include <string.h>
#include "./GameObjectsDir/EnvironmentsDir/Environment.h"
#include "./GameObjectsDir/EnvironmentsDir/ImpassableTerrain.h"
#include "./GameObjectsDir/EnvironmentsDir/Pit.h"
#include "./GameObjectsDir/ItemsDir/Gold.h"
#include "./GameObjectsDir/CharactersDir/Hero.h"
#include "./GameObjectsDir/CharactersDir/Wumpus.h"
#include "./BehavioursDir/PerceptionsDir/Breeze.h"
#include "./BehavioursDir/PerceptionsDir/Bump.h"
#include "./BehavioursDir/PerceptionsDir/Glitter.h"
#include "./BehavioursDir/PerceptionsDir/Scream.h"
#include "./BehavioursDir/PerceptionsDir/Stench.h"




class MapGrid
{
	int length;
	int height;
	int numPits;
	int numBlank;

	Environment ***grid;
	Hero hero;


	public:
		MapGrid ();
		MapGrid(int inUserID);
		MapGrid (int inX, int inY, int inUserID, int inHeroHealth);
		MapGrid (int inX, int inY, int inUserID);
		virtual ~MapGrid ();

		char *toString ();
		Environment ***getMapGrid ();
		Environment *getTile(int inXCoord, int inYCoord);
		void generateMapGrid ();
		void createBorder ();
		int spawnGold ();
		int spawnWumpus ();
		int placePits ();
		int getGridDimensions(int *outLength, int *outHeight);
		int coordinateToAbsolute (int *outXCoord, int *outYCoord);
		int absoluteToCoordinate (int *outXCoord, int *outYCoord);

};

#endif /*MAP_GRID_HEADER*/