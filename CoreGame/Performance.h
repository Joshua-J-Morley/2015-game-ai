/**
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 04 2015
 * 
 * @brief modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: Implement performance into mocked functionality game
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#ifndef AI_GAME_PROJECT_PERFORMANCE_H
#define AI_GAME_PROJECT_PERFORMANCE_H

#include <string>

class Performance {
	int gold;
	int death;
	int step;
	int arrow;
	int score;

public:
	Performance();

	Performance(int inGold, int inDeath, int inStep, int inArrow);

	virtual ~Performance();

	int getScore();

	void updateScore(int value);

	void getPerformanceFromFile(std::string file);

	
	void heroMoved();
	void heroGrab(int result);
	void heroShoot();
	void heroDied();
	void heroFoundGold();
};

#endif //AI_GAME_PROJECT_PERFORMANCE_H