/**
 * @class Performance
 * @file Performance.cpp
 * Class for the performance metric, to measure the agent's performance as a score value
 *
 * @brief created by
 * @author Adrian Rasmussen <15514868@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 7 2015
 * 
 * For intermediate modifications see @ref Performance.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */
#include "Performance.h"

/**
 * @brief Default constructor, sets performance measures to default values according to Artificial Intelligence A Modern Approach
 */
Performance::Performance() 
{
	gold = 1000;
	death = -1000;
	step = -1;
	arrow = -10;
	score = 0;
}

/**
 * @brief Alternate constructor, sets values according to input parameters
 *
 * @param[in]   gold    value representing score received for finding gold
 * @param[in]   death   negative score received for agent death (from Wumpus or pit)
 * @param[in]   step    negative score for each action or step taken by the agent
 * @param[in]   arrow   negative score for using up the only arrow, by shooting it
 */
Performance::Performance(int inGold, int inDeath, int inStep, int inArrow) 
{
	gold = inGold;
	death = inDeath;
	step = inStep;
	arrow = inArrow;
	score = 0;
}

/**
 * @brief Deconstructor
 */
Performance::~Performance() 
{

}


/**
 * @brief returns the current score for the agent
 */
int Performance::getScore() 
{
	return score;
}

/**
 * @brief Sets the score values for each metric using a given config file
 *
 * @param[in]   file    config file containing score values
 * Must be of the form:
 * metric #,
 * metric2 #
 *
 * for example:
 * gold 1000,
 * death -1000,
 * step -1,
 * arrow -10
 */
void Performance::getPerformanceFromFile(std::string file) 
{

}

/**
 * @brief Adds the supplied value to the total score
 *
 * @param   value   integer representing value to be added, may be negative
 */
void Performance::updateScore(int value) 
{
	score += value;
}

/**
 * @brief      update heroes score when hero has moved
 */
void Performance::heroMoved()
{
	score = score -1;
}

/**
 * @brief      update heroes score when her has attempted a grab
 *
 * @param[in]  result  result of grab attempt (IE found gold or did not)
 */
void Performance::heroGrab(int result)
{
	if (result == 0)
	{
		score = score -1;
	}
	else if (result == 1)
	{
		heroFoundGold();
	}
}


/**
 * @brief      update heroes score when hero has shot arrow
 */
void Performance::heroShoot()
{
	score = score -10;
}

/**
 * @brief      update heroes score when hero has died
 */
void Performance::heroDied()
{
	score = score -1000;
}

/**
 * @brief      update heroes score when hero has found the gold
 */
void Performance::heroFoundGold()
{
	score = score +1000;
}