/**
 * @class Enviroment
 * @file Enviroment.cpp
 * Base class for elements of grid
 * inherits from @ref GameObject
 * Inherited by 
 * - @ref SpawnTerrain
 * - @ref ImpassableTerrain
 * - @ref Pit
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref Environment.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */
#include <stdio.h>
#include "Environment.h"

/**
 * @brief      Default Constructor
 */
Environment::Environment () : GameObject ()
{
	passable = 1; 	///< 0 for unpassable, 1 for passable
	hazardous = 0;	///< 0 for non hazardous, 1 for hazardous

}

/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX   x coordinate of Environment element
 * @param[in]  inY   x coordinate of Environment element
 */
Environment::Environment (int inX, int inY) : GameObject (inX, inY)
{
	xCoord = inX;
	yCoord = inY;

	passable = 1;
	hazardous = 0;
}



/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX          x coordinate of Environment element
 * @param[in]  inY          x coordinate of Environment element
 * @param[in]  inPassable   value indicating if this element is passable 0 for
 *                          false 1 for true
 * @param[in]  inHazardous  value indicating if this element is hazardous 0 for
 *                          false 1 for true
 */
Environment::Environment (int inX, int inY, int inPassable, int inHazardous) 
							: GameObject ( inX, inY)
{
	xCoord = inX;
	yCoord = inY;

	passable = inPassable;
	hazardous = inHazardous;
}

/**
 * @brief      Destructor
 */
Environment::~Environment()
{

}

/**
 * @brief      Accessor to return the coordinates of this element.
 *
 * @param[in,out]      outXCoord  x coordinate of element returned by reference
 * @param[in,out]      outYCoord  y coordinate of element returned by reference
 */
void Environment::getCoordinates(int *outXCoord, int *outYCoord)
{
	*outXCoord = xCoord;
	*outYCoord = yCoord;
}

/**
 * @brief      Method indicating if this @ref Environment element is passable.
 *
 * @return     Value of passable (overridden by subclasses).
 */
int Environment::isPassable()
{
	return passable;
}

/**
 * @brief      Method indicating if this @ref Environment element is hazardous
 *
 * @return     Value of hazardous (overridden by subclasses).
 */
int Environment::isHazard()
{
	return hazardous;
}

/**
 * @brief      add a new object to this environment tile
 *
 * @param[in]  inObject  gameobject to be added
 *
 * @return     result of adding object
 */
int Environment::addObject(GameObject inObject)
{
	objectsList.push_back(inObject);
}

/**
 * @brief      return char array with letters representing objects on tile
 *
 * @return     char array of objects on this environment tile
 */
char *Environment::getObjects()
{
	char *retString;
	std::stringstream strStream;
	std::list<GameObject>::iterator lIter;

	for (lIter = objectsList.begin(); lIter != objectsList.end(); lIter++)
	{
		char type = lIter->getType();
		/*printf("char:%c||\t", type);*/
		strStream << type;
	}

	retString = (char*) malloc(sizeof(strStream.str().c_str())*sizeof(char)+1);
	strcpy(retString, strStream.str().c_str());

	/*printf("retstring %s: %s;\n", retString, strStream.str().c_str());*/
	return retString;
}

int Environment::addPerception(Percept inPercept)
{
	perceptionList.push_back(inPercept);
}

/**
 * @brief      return char array with letters representing objects on tile
 *
 * @return     char array of objects on this environment tile
 */
char *Environment::percieve()
{
	char *retString;
	std::stringstream strStream;
	std::list<Percept>::iterator lIter;

	for (lIter = perceptionList.begin(); lIter != perceptionList.end(); lIter++)
	{
		char type = lIter->perceive();
		/*printf("char:%c||\t", type);*/
		strStream << type;
	}

	retString = (char*) malloc(sizeof(strStream.str().c_str())*sizeof(char)+1);
	strcpy(retString, strStream.str().c_str());
	if (strstr(retString, "H") != NULL)
	{
		printf("You Smell a foul Stench\n");
	}
	if (strstr(retString, "Z") != NULL)
	{
		printf("You feel a strong breeze\n");
	}
	if (strstr(retString, "H") != NULL)
	{
		printf("You see a bright, golden glitter in the room\n");
	}
	/*printf("percieve %s: %s;\n", retString, strStream.str().c_str());*/
	return retString;
}

/**
 * @brief      function to be overwritten by subclasses
 */
int Environment::process()
{

}