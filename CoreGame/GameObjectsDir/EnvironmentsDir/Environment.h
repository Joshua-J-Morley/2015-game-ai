 /**
 * @class Enviroment
 * @file Enviroment.h
 * Base class for elements of grid
 * inherits from @ref GameObject
 * Inherited by 
 * - @ref SpawnTerrain
 * - @ref BlankTerrain
 * - @ref ImpassableTerrain
 * - @ref Pit
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: 
 */

#ifndef ENVIRONMENT_HEADER
#define ENVIRONMENT_HEADER

#include <sstream>
#include <string.h>
#include <stdlib.h>
#include "../GameObject.h"

class Environment : public GameObject
{
	protected:
		int passable; //0 for unpassable, 1 for passable
		int hazardous;
		std::list<GameObject> objectsList;

	
	public:
		Environment ();
		Environment (int inX, int inY);
		Environment (int inX, int inY, int inPassable, int inHazardous);
		virtual ~Environment();

		void getCoordinates(int *xCoord, int *yCoord);
		virtual int isPassable();
		virtual int isHazard();

		int addObject(GameObject inObject);
		int getNumOjects();
		char *getObjects();
		int getObject(int index);
		
		int addPerception(Percept inPercept);
		char *percieve();
		int process ();

};

#endif /*ENVIRONMENT_HEADER*/