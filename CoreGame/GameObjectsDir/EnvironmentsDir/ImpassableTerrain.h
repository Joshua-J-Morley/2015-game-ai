 /**
 * @class ImpassableTerrain
 * @file ImpassableTerrain.h
 * class for impassable terrain on grid
 * inherits from @ref Environment
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: 
 */

#ifndef IMPASSABLE_TERRAIN_HEADER
#define IMPASSABLE_TERRAIN_HEADER

#include "Environment.h"

class ImpassableTerrain : public Environment
{
	public:
		ImpassableTerrain ();
		ImpassableTerrain (int inX, int inY);
		virtual ~ImpassableTerrain();

		int isPassable();
		int isHazard();
};

#endif /*IMPASSABLE_TERRAIN_HEADER*/