/**
 * @class Pit
 * @file Pit.cpp
 * class for a pit on the grid
 * inherits from @ref Environment
 * 
 * @brief created by
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date sep 2015
 * 
 * For intermediate modifications see @ref Pit.h
 * 
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "Pit.h"

/**
 * @brief      Default Constructor
 */
Pit::Pit () : Environment()
{

}

/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX   x coordinate of pit.
 * @param[in]  inY   y coordinate of pit.
 */
Pit::Pit (int inX, int inY) : Environment (inX, inY, 0, 1)
{

}

/**
 * @brief      Destructor
 */
Pit::~Pit()
{

}

/**
 * @brief      Method indicating if this @ref Environment element is passable.
 *
 * @return     Value of passable (for Pit == 1 indicating is passable).
 */
int Pit::isPassable()
{
	return passable;
}

/**
 * @brief      Method indicating if this @ref Environment element is hazardous
 *
 * @return     Value of hazard (For Pit == 1 indicating hazardous).
 */
int Pit::isHazard()
{
	return hazardous;
}
