 /**
 * @class ImpassableTerrain
 * @file ImpassableTerrain.cpp
 * class for impassable terrain on grid
 * inherits from @ref Environment
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref ImpassableTerrain.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "ImpassableTerrain.h"

/**
 * @brief      Default Constructor
 */
ImpassableTerrain::ImpassableTerrain () : Environment()
{

}

/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX   x coordinate of impassable terrain object
 * @param[in]  inY   y coordinate of impassable terrain object
 */
ImpassableTerrain::ImpassableTerrain (int inX, int inY) 
										: Environment (inX, inY, 0, 0)
{

}

/**
 * @brief      Destructor
 */
ImpassableTerrain::~ImpassableTerrain()
{

}

/**
 * @brief      Method indicating if this @ref Environment element is passable.
 *
 * @return     Value of passable (for ImpassableTerrain == 0 indicating is not passable).
 */
int ImpassableTerrain::isPassable()
{
	return passable;
}

/**
 * @brief      Method indicating if this @ref Environment element is hazardous
 *
 * @return     Value of hazard (For ImpassableTerrain == 0 indicating not hazardous).
 */
int ImpassableTerrain::isHazard()
{
	return hazardous;
}
