/**
 * @class GameObject
 * @file GameObject.h
 * Base class for elements of grid
 * Inherited by 
 * - @ref Enviroment
 * - @ref Character
 * - @ref Item
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: 
 */

#ifndef GAME_OBJECT_HEADER
#define GAME_OBJECT_HEADER

#include <list>
#include "../BehavioursDir/PerceptionsDir/Percept.h"
#include "../BehavioursDir/ActionsDir/Action.h"


class GameObject
{
	protected:
		std::list<Percept> perceptionList;
		std::list<Behaviour> actionList;
		int xCoord;
		int yCoord;
		char objType;

	public:
		GameObject ();
		GameObject (char inObjType);
		GameObject (int inX, int inY);
		GameObject (int inX, int inY, char inObjType);
		virtual ~GameObject();

		/*int addAction(Action inAction);*/
		void getCoordinates (int *outXCoord, int *outYCoord);
		void setCoordinates (int inXCoord, int inYCoord);
		int getXCoord ();
		int getYCoord ();
		virtual int process();
		char getType ();


};

#endif /*GAME_OBJECT_HEADER*/