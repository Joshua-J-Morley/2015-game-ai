/**
 * @class Gold
 * @file Gold.cpp
 * class for a Gold on the grid
 * inherits from @ref Item
 * 
 * 
 * @brief created by
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date sep 2015
 * 
 * 
 * @brief last modified
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0.1
 * @date Oct 20 2015
 */

#include "Gold.h"

/**
 * @brief      Default Constructor
 */
Gold::Gold () : Item('G')
{

}

/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX   x coordinate of Gold.
 * @param[in]  inY   y coordinate of Gold.
 */
Gold::Gold (int inX, int inY) : Item (inX, inY, 'G')
{

}

/**
 * @brief      Destructor
 */
Gold::~Gold()
{

}

/**
 * @brief      function to be overwritten by subclasses
 */
int Gold::process()
{

}