 /**
 * @class Item 
 * @file Item.cpp
 * Base class for Items in game
  * inherits from @ref GameObject
 * Inherited by 
 * - @ref Hero
 * - @ref Wumpus
 * 
 * @brief created by
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0.1
 * @date Oct 20 2015
 */

#include "Item.h"

/**
 * @brief      Default Constructor
 */
Item::Item () : GameObject ()
{
	xCoord = 0;
	yCoord = 0;
}

/**
 * @brief      Default Constructor
 *
 * @param[in]  inItemType  char representing what type of item this item is
 */
Item::Item (char inItemType) : GameObject (inItemType)
{
	xCoord = 0;
	yCoord = 0;
}

/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX   x coordinate of Item 
 * @param[in]  inY   y coordinate of Item 
 */
Item::Item (int inX, int inY) : GameObject (inX, inY)
{
	xCoord = inX;
	yCoord = inY;
}

/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX         x coordinate of Item
 * @param[in]  inY         y coordinate of Item
 * @param[in]  inItemType  char representing what type of item this item is
 */
Item::Item (int inX, int inY, char inItemType) : GameObject (inX, inY, inItemType)
{
	xCoord = inX;
	yCoord = inY;
}

/**
 * @brief      Destructor
 */
Item::~Item()
{

}

/**
 * @brief      Accessor to return the location of this Item.
 *
 * @param[in,out]      outXCoord  x coordinate of item returned by reference
 * @param[in,out]      outYCoord  y coordinate of item returned by reference
 */
void Item::getLocation(int *outXCoord, int *outYCoord)
{
	*outXCoord = xCoord;
	*outYCoord = yCoord;
}


/**
 * @brief      function to be overwritten by subclasses
 */
int Item::process()
{

}