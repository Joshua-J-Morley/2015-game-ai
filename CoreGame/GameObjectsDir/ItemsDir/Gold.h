/**
 * @class Gold
 * @file Gold.h
 * class for a Gold on the grid
 * inherits from @ref Item
 * 
 * @brief created by
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date sep 2015
 * 
 * @brief last modified
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0.1
 * @date Oct 20 2015
 */

#ifndef GOLD_HEADER
#define GOLD_HEADER

#include "Item.h"

class Gold : public Item
{
	public:
		Gold ();
		Gold (int inX, int inY);
		virtual ~Gold ();
		int process ();
};

#endif /*GOLD_HEADER*/