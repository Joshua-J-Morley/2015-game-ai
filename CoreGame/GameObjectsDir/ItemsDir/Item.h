 /**
 * @class Item 
 * @file Item.h
 * Base class for Items in game
  * inherits from @ref GameObject
 * Inherited by 
 * - @ref Hero
 * - @ref Wumpus
 * 
 * @brief created by
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0.1
 * @date Oct 20 2015
 */

#ifndef ITEM_HEADER
#define ITEM_HEADER

#include "../GameObject.h"

class Item : public GameObject
{
	public:
		Item ();
		Item (char inItemType);
		Item (int inX, int inY);
		Item (int inX, int inY, char inItemType);
		virtual ~Item();

		void getLocation(int *xCoord, int *yCoord);
		virtual int process();
};

#endif /*ITEM_HEADER*/