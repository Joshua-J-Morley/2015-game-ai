 /**
 * @class Hero
 * @file Hero.cpp
 * class for Hero in game
 * Inherits from @ref Character
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref Hero.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "Hero.h"

/**
 * @brief      Default Constructor
 */
Hero::Hero () : Character ('H')
{
	heroHealth = 0;
	heroID = 0;
	directionFaced = 1;
}

/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX           x coordinate of hero
 * @param[in]  inY           y coordinate of hero
 * @param[in]  inHeroID      Heros Identification number
 * @param[in]  inHeroHealth  Heroes health
 */
Hero::Hero (int inX, int inY, int inHeroID, int inHeroHealth) : Character (inX, inY, 'H')
{
	heroID = inHeroID;
	heroHealth = inHeroHealth;
	directionFaced = 1;
}


/**
 * @brief      Destructor
 */
Hero::~Hero()
{

}


/**
 * @brief      accessor for direction faced by hero
 *
 * @return     direction her is facing
 */
int Hero::getDirection()
{
	return directionFaced;
}

/**
 * @brief      Mutator for direction faced	
 *
 * @param[in]  inDirectionFaced  new direction
 *
 * @return     result of mutate attempt
 */
int Hero::setDirection(int inDirectionFaced)
{
	directionFaced = inDirectionFaced;
}

/**
 * @brief      stub to allow for scripting hero at later stage
 *
 * @param[in]  inAction  action to be added to heroes action list
 *
 * @return     result of add attempt
 */
int Hero::addAction(Behaviour inAction)
{
	actionList.push_back(inAction);
}

/**
 * @brief      do actions
 *
 * @return     result of action
 * @todo figure out how to do list iteration with polymorphism calls
 */
int Hero::doActions ()
{
/*std::list<Behaviour>::iterator lIter;

	for (lIter = actionList.begin(); lIter != actionList.end(); lIter++)
	{
		lIter->tick(this);
	}*/
}

/**
 * @brief      stubbed functionality until polymorphism is implemented
 *
 * @param[in]  result  result of attempting grab
 *
 * @return     result of grab attempt
 */
int Hero::doGrab (int result)
{
	performance.heroGrab(result);
}


/**
 * @brief      stubbed functioanlitry until polymorphism is implemented
 *
 * @return     result of attempting shoot
 */
int Hero::doShoot ()
{
	static int arrows = 1;
	if (arrows == 1)
	{
		performance.heroShoot();
	}
	else
	{
		performance.heroShoot();
	}
	
}

/**
 * @brief      stub for hero performing a turn action 
 *
 * @return     result of action attempt
 */
int Hero::doTurn ()
{
	
}

/**
 * @brief      stub for when hero actions is abstracted out from map
 *
 * @param[in]  result  result of attempting action
 *
 * @return     result of attempting move
 */
int Hero::doMove (int result)
{
	performance.heroMoved();
}


/**
 * @brief      function to be overwritten by subclasses
 *
 * @return     result of action
 */
int Hero::process()
{

}