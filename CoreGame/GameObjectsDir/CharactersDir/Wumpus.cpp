 /**
 * @class Wumpus
 * @file Wumpus.cpp
 * class for a wumpus on the grid
 * inherits from @ref Character
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref Wumpus.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "Wumpus.h"
 
/**
 * @brief      Default Constructor
 */
Wumpus::Wumpus () : Character('W')
{

}


/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX   x coordinate of wumpus.
 * @param[in]  inY   y coordinate of wumpus.
 */
Wumpus::Wumpus (int inX, int inY) : Character (inX, inY, 'W')
{
	wumpusHealth = 1;
}


/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX             x coordinate of wumpus.
 * @param[in]  inY             y coordinate of wumpus.
 * @param[in]  inWumpusHealth  health of wumpus
 */
Wumpus::Wumpus (int inX, int inY, int inWumpusHealth) : Character (inX, inY, 'W')
{
	wumpusHealth = inWumpusHealth;
}

/**
 * @brief      Destructor
 */
Wumpus::~Wumpus()
{

}

/**
 * @brief      function to be overwritten by subclasses
 */
int Wumpus::process()
{

}