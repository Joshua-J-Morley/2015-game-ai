 /**
 * @class Hero
 * @file Hero.h
 * class for Hero in game
 * Inherits from @ref Character
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: 
 */

#ifndef HERO_HEADER
#define HERO_HEADER

#include "../../BehavioursDir/ActionsDir/Grab.h"
#include "../../BehavioursDir/ActionsDir/Shoot.h"
#include "../../BehavioursDir/ActionsDir/Move.h"
#include "../../BehavioursDir/ActionsDir/Turn.h"
 #include "../../Performance.h"
#include "Character.h"

class MapGrid;


class Hero : public Character
{
	/*MapGrid *mapGrid;*/
	Performance performance;
	int heroHealth;
	int heroID;
	int directionFaced;

	public:
		Hero ();
		Hero (int inX, int inY, int inHeroID, int inHeroHealth);
		virtual ~Hero();

		int getDirection();
		int setDirection(int inDirectionFaced);

		int addAction(Behaviour inAction);
		int doActions();
		int doAction();
		int process();
	
		int doGrab (int result);	
		int doShoot ();	
		int doTurn ();	
		int doMove (int result);

};

#endif /*HERO_HEADER*/