/**
 * @class Character
 * @file Character.cpp
 * inherits from @ref GameObject
 * Inherited by 
 * - @ref Hero
 * - @ref Wumpus
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: 
 */

#ifndef CHARACTER_HEADER
#define CHARACTER_HEADER

#include "../GameObject.h"

class Character : public GameObject
{
	

	public:
		Character ();
		Character (char inCharacType);
		Character (int inX, int inY);
		Character (int inX, int inY, char inCharacType);
		virtual ~Character();
		virtual int process();
};

#endif /*CHARACTER_HEADER*/