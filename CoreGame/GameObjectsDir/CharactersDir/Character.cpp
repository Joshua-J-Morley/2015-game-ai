/**
 * @class Character
 * @file Character.cpp
 * inherits from @ref GameObject
 * Inherited by 
 * - @ref Hero
 * - @ref Wumpus
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref Character.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "Character.h"

/**
 * @brief      Default Constructor
 */
Character::Character () : GameObject ()
{

}

/**
 * @brief      Alternate Constructor for character type	
 *
 * @param[in]  inCharacType  character type (W or H)
 */
Character::Character (char inCharacType) : GameObject (inCharacType)
{

}

/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX   x coordinate of Character element
 * @param[in]  inY   y coordinate of Character element
 */
Character::Character (int inX, int inY) : GameObject (inX, inY)
{
	
}


/**
 * @brief      Alternate Constructor
 *
 * @param[in]  inX           x coordinate of Character element
 * @param[in]  inY           y coordinate of Character element
 * @param[in]  inCharacType  character type (W or H)
 */
Character::Character (int inX, int inY, char inCharacType) 
							: GameObject (inX, inY, inCharacType)
{
	
}

/**
 * @brief      Destructor
 */
Character::~Character()
{

}


/**
 * @brief      function to be overwritten by subclasses
 */
int Character::process()
{

}