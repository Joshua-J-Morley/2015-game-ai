 /**
 * @class Wumpus
 * @file Wumpus.h
 * class for a wumpus on the grid
 * inherits from @ref Character
  * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * @brief Modified By:
 * @author 
 * @version 
 * @date 
 * @brief Reason:
 * 
 * @brief last modified by:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 * @brief Reason: 
 */

#ifndef WUMPUS_HEADER
#define WUMPUS_HEADER

#include "Character.h"

class Wumpus : public Character
{
	int wumpusHealth;

	public:
		Wumpus ();
		Wumpus (int inX, int inY);
		Wumpus (int inX, int inY, int inWumpusHealth);
		virtual ~Wumpus ();
		int process ();
};

#endif /*WUMPUS_HEADER*/