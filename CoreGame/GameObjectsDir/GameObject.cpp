/**
 * @class GameObject
 * @file GameObject.cpp
 * Base class for elements of grid
 * Inherited by 
 * - @ref Enviroment
 * - @ref Character
 * - @ref Item
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref GameObject.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */
 
#include "GameObject.h"

/**
 * @brief      Default Constructor
 */
GameObject::GameObject ()
{
	xCoord = 0;
	yCoord = 0;
}

/**
 * @brief      Default Constructor
 *
 * @param[in]  inObjType  char representing type of game object
 */
GameObject::GameObject (char inObjType)
{
	xCoord = 0;
	yCoord = 0;
	objType = inObjType;
}

/**
 * @brief      Default Constructor
 *
 * @param[in]  inX   x coordinate of this game object
 * @param[in]  inY   y coordiante of this game object
 */
GameObject::GameObject (int inX, int inY)
{
	xCoord = inX;
	yCoord = inY;
}


/**
 * @brief      Default Constructor
 *
 * @param[in]  inX        x coordinate of this object
 * @param[in]  inY        y coordainte of this object
 * @param[in]  inObjType  char representing type of game object
 */
GameObject::GameObject (int inX, int inY, char inObjType)
{
	xCoord = inX;
	yCoord = inY;
	objType = inObjType;
}

/**
 * @brief      Destructor
 */
GameObject::~GameObject ()
{

}

/*int GameObject::addAction(Action inAction)
{
	actionList.push_back(inAction);
}*/

/**
 * @brief         return x and y coordinates of this object by reference
 *
 * @param[in,out] outXCoord  x coordinate of this object
 * @param[in,out] outYCoord  y coordinate of this object
 */
void GameObject::getCoordinates (int *outXCoord, int *outYCoord)
{
	*outXCoord = xCoord;
	*outYCoord = yCoord;
}

/**
 * @brief         return x and y coordinates of this object by reference
 *
 * @param[in,out] outXCoord  x coordinate of this object
 * @param[in,out] outYCoord  y coordinate of this object
 */
void GameObject::setCoordinates (int inXCoord, int inYCoord)
{
	xCoord = inXCoord;
	yCoord = inYCoord;
}

/**
 * @brief      accessor for x coordinate
 *
 * @return     x coordinate of this object
 */
int GameObject::getXCoord ()
{
	return xCoord;
}

/**
 * @brief      accessor for y coordinate
 *
 * @return     y coordinate of this object
 */
int GameObject::getYCoord ()
{
	return yCoord;
}

/**
 * @brief      get type of object 
 *
 * @return     type of object
 */
char GameObject::getType ()
{
	return objType;
}

/**
 * @brief      function to be overwritten by subclasses
 */
int GameObject::process ()
{

}


