#include "CoreGame/GameObjectsDir/CharactersDir/Hero.h"


#include "CoreGame/BehavioursDir/ActionsDir/Action.h"
#include "CoreGame/BehavioursDir/ActionsDir/Turn.h"
#include "CoreGame/BehavioursDir/ActionsDir/Move.h"
#include "CoreGame/BehavioursDir/ActionsDir/Grab.h"
#include "CoreGame/BehavioursDir/ActionsDir/Shoot.h"

#include "./CoreGame/MapGrid.h"

class Controller
{
	MapGrid mapGrid;
	Hero hero;


	public:
		Controller();
		Controller(int inUserID);
		int run();
		int signalMove();
		int signalTurn();
		int signalGrab();
		int signalShoot();
		int signalDisp();

};


