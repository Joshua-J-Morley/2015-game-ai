/**
 * @class main
 * @file main.cpp
 * Class for main, 
 * this class just provides stubbed functionality for demonstration purposes
 * 
 * @brief Created By
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
 * For intermediate modifications see @ref main.h
 * 
 * @brief Last Modified By:
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date Oct 2015
 */

#include "main.h"

/**
 * @brief      main function, provides mocked functionality
 *
 * @param[in]  argc  argument count
 * @param      argv  argument vector
 *
 * @return     results of execution
 */
int main(int argc, char const *argv[])
{
	/*std::vector<std::string> moves;*/ 
	/*beginging of extension path finding*/
	int result, finished = 0;
	Controller cont;
	char* command = (char*)malloc(7);

	/*loop until user opts to exit*/
	do
	{
		printf("Enter Command: (or help)\n");
		scanf("%7s", command);
		
		if(strcmp(command, "move") == 0)
		{
			result = cont.signalMove();
			/*moves.push_back("move")*/
			/*beginging of extension path finding*/
		}
		else if(strcmp(command, "turn") == 0)
		{				
			result = cont.signalTurn();
		}
		else if(strcmp(command, "grab") == 0)
		{
			result = cont.signalGrab();
		}
		else if(strcmp(command, "shoot") == 0)
		{
			result = cont.signalShoot();
		}
		else if(strcmp(command, "quit") == 0)
		{
			finished = 1;
		}
		else if (strcmp(command, "disp") == 0)
		{
			cont.signalDisp();
		}
		else
		{
			printf("invalid choice choose from:\n");
			printf("\tgrab\n");
			printf("\tshoot\n");
			printf("\tturn\n");
			printf("\tmove\n");
			printf("\tquit\n");
		}
		if (result == 1)
		{
			finished = 1;
		}
	}while(finished == 0);
	
	return 0;
}