 /**
 * @class Controller
 * @file Controller.cpp
 * Class to demonstrate and test basic and mocked functionality
 * 
  * 
 * @brief created by
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0
 * @date oct 2015
 * 
  * 
 * @brief last modified
 * @author Joshua Morley <15490010@student.curtin.edu.au>
 * @version 1.0.1
 * @date Oct 2015
 */

#include "Controller.h"

Controller::Controller()
{
	mapGrid= MapGrid(1);
	mapGrid.generateMapGrid();

	hero = Hero(0, 0, 1, 1);
}

Controller::Controller(int inUserID)
{
	mapGrid= MapGrid(inUserID);
	mapGrid.generateMapGrid();

	hero = Hero(0, 0, inUserID, 1);

}

int Controller::run()
{
	int heroX, heroY;

	hero.getCoordinates(&heroX, &heroY);

	Turn turn = Turn();
	Move move = Move();
	Grab grab = Grab();
	Shoot shoot = Shoot();

	hero.setDirection(turn.turnRight(hero.getDirection()));

	move.moveOne(&heroX, &heroY, hero.getDirection());
	hero.setCoordinates(heroX, heroY);
	
	move.moveOne(&heroX, &heroY, hero.getDirection());
	hero.setCoordinates(heroX, heroY);

	hero.setDirection(turn.turnLeft(hero.getDirection()));

	move.moveOne(&heroX, &heroY, hero.getDirection());
	hero.setCoordinates(heroX, heroY);

	hero.setDirection(turn.turnLeft(hero.getDirection()));

	move.moveOne(&heroX, &heroY, hero.getDirection());
	hero.setCoordinates(heroX, heroY);

	grab.grabItem(mapGrid.getTile(heroX, heroY)->getObjects());
}


int Controller::signalMove()
{
	int distance, heroX, heroY, result = 0;
	Move move = Move();

	hero.getCoordinates(&heroX, &heroY);+


	printf("how many squares?\n");
	scanf("%d", &distance);

	for (int ii = 0; ii < distance; ii++)
	{
		move.moveOne(&heroX, &heroY, hero.getDirection());
		
		Environment *tile = mapGrid.getTile(heroX, heroY);

		if (tile->isPassable() == 0)
		{
			if (tile->isHazard() == 1)
			{
				printf("Hero fell screeming into the dark abyss\n");
				result = 1;
				ii = distance;
			}
			else
			{
				printf("bump\n");
			}
		}
		else
		{
			hero.setCoordinates(heroX, heroY);

			printf("Moved to: %d, %d\n", heroX, heroY);
			tile->percieve();

		}
	}

	return result;
}

int Controller::signalTurn()
{
	char* direction = (char*)malloc(7);
	Turn turn = Turn();

	printf("Turn left, right or around?\n");
	scanf("%7s", direction);

	if(strcmp(direction, "left") == 0)
	{
		hero.setDirection(turn.turnLeft(hero.getDirection()));
	}
	else if(strcmp(direction, "right") == 0)
	{
		hero.setDirection(turn.turnRight(hero.getDirection()));
	}
	else if(strcmp(direction, "around") == 0)
	{
		hero.setDirection(turn.turnLeft(hero.getDirection()));
		hero.setDirection(turn.turnLeft(hero.getDirection()));
	}

	return 0;
}

int Controller::signalGrab()
{
	int heroX, heroY;
	Grab grab = Grab();

	hero.getCoordinates(&heroX, &heroY);

	grab.grabItem(mapGrid.getTile(heroX, heroY)->getObjects());

	return 0;
}

int Controller::signalShoot()
{
	int heroX, heroY, shootX, shootY;
	hero.getCoordinates(&heroX, &heroY);

	shootX = heroX, shootY = heroY;
	Shoot shoot = Shoot();

	shoot.shootArrow(&shootX, &shootY, hero.getDirection());
	shoot.isHit(mapGrid.getTile(shootX, shootY)->getObjects());

	return 0;
}

int Controller::signalDisp()
{
	mapGrid.toString();
}
