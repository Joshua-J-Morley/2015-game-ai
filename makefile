# @file makefile
# makefile to compile program
# 
# @brief Creator
# @author Joshua Morley <15490010@student.curtin.edu.au>
# @version 1.0
# @date sep 2015
# 
# @brief Modified by:
# @author
# @version
# @date
# @brief Reason:
# 
# @brief Last Modified by:
# @author 
# @version 1.0
# @date 


#make variables used in makefile
CPP = g++ -std=c++0x
EXEC = run
DIR = ./obj



#make variables to store directory tree as path for vpath to search for source
COREGAME = ./CoreGame
BEHAVIOURS = ./CoreGame/BehavioursDir
ACTIONS = $(BEHAVIOURS)/ActionsDir
PERCEPTIONS = $(BEHAVIOURS)/PerceptionsDir
ALLBEHAVIOURS = $(BEHAVIOURS) $(ACTIONS) $(PERCEPTIONS)
GAMEOBJECTS= ./CoreGame/GameObjectsDir
CHARACTERS= $(GAMEOBJECTS)/CharactersDir
ENVIRONMENTS= $(GAMEOBJECTS)/EnvironmentsDir
ITEMS = $(GAMEOBJECTS)/ItemsDir
ALLGOBJECTS =  $(GAMEOBJECTS) $(CHARACTERS) $(ENVIRONMENTS) $(ITEMS)

# vpath varibale stores path to all possible source code in current directory tree
ALLPATH =  $(ROOTSOURCES) $(COREGAME) $(ALLBEHAVIOURS) $(ALLGOBJECTS)
VPATH = $(ALLPATH)


#wilcard will find all cpp files in subdirectories of ActionsDIr
ACTIONSSOURCE = $(wildcard $(ACTIONS)/*.cpp) 
PERCEPTIONSSOURCE = $(wildcard $(PERCEPTIONS)/*.cpp) 
BEHAVIOURSSOURCE = $(wildcard $(BEHAVIOURS)/*.cpp)  $(ACTIONSSOURCE) $(PERCEPTIONSSOURCE)

#wilcard will find all cpp files in subdirectories of CharactersDir
CHARACTERSSOURCE = $(wildcard $(CHARACTERS)/*.cpp) 
ENVIRONMENTSSOURCE = $(wildcard $(ENVIRONMENTS)/*.cpp) 
ITEMSSOURCE = $(wildcard $(ITEMS)/*.cpp)
GAMEOBJECTSSOURCE = $(wildcard $(GAMEOBJECTS)/*.cpp) $(CHARACTERSSOURCE) $(ENVIRONMENTSSOURCE) $(ITEMSSOURCE)

#wilcard will find all cpp files in coregame directory
COREGAMESOURCES = $(wildcard ./CoreGame/*.cpp) $(BEHAVIOURSSOURCE) $(GAMEOBJECTSSOURCE)
#wilcard will find all cpp files in root directory
ROOTSOURCES = $(wildcard *.cpp) 

#makefile variable will combine all source files into 1 make variable
ALLSOURCES =  $(ROOTSOURCES) $(COREGAMESOURCES) 



#ALLSOURCES = $(wildcard *.cpp) $(wildcard **/*.cpp)

OBJECTS = $(ALLSOURCES:.cpp=.o)
DEPS = $(ALLSOURCES:.cpp=.d)
BINS = $(ALLSOURCES:.cpp=)

#link objects
$(EXEC):$(OBJECTS)
	@echo Linking
	@$(CPP) $(OBJECTS) -o $@
	@echo


# To obtain object files
# compile .cpp files using previously stated compiler flags
$(DIR)/%.o: %.cpp
	@echo Compiling $@
	@$(CPP) -c -o $@ $<
	@echo
	@echo
	@echo
	@echo


# To remove generated files
clean:
	@rm -f $(OBJECTS)


# To remove generated files including executable
cleanall:
	@rm -f $(OBJECTS) run